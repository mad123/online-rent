const mongoose = require('mongoose');
const {mongoUri} = require('./kyes');
const option = { useNewUrlParser: true, useUnifiedTopology: true }

mongoose.connect(mongoUri, option, (err, db) => {
    if (err) throw err;
    console.log("Database created!");
});