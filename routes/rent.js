const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Rent = mongoose.model("Rent");

// ====================== get rent item by id or get all rent items start =========================
    router.get("/rent/items/:id?", (req, res) => {
        var findData = req.params.id == undefined ? Rent.find() : Rent.find({_id: req.params.id});

        findData.then(items => {
            return res.json({ items })
        }).catch(err => {console.log('err', err) })
    });
// ====================== get rent item by id or get all rent items end =============================

// =========================== add rent item code start ========================================
    router.post("/add/rent/item/:id?", (req, res) => {
        var { userId, itemId, quantity, start_date, end_date } = req.body;

        if(!itemId || !quantity || !start_date || !end_date) {
            return res.status(422).json({error: "all fields are required"});
        } else {
            if(req.params.id) {
                Rent.updateOne(
                    { _id: req.params.id }, 
                    { $set: { itemId, quantity, start_date, end_date } } 
                ).then(up => {
                    return res.json({ success: true, message: "sucessfully update" })
                })
            } else {
                const rent = new Rent({ userId, itemId, quantity, start_date, end_date });
                rent.save().then(item => {
                    return res.json({ success: true, message: "sucessfully added for rent" })
                }).catch(err => {console.log('save err', err) })
            }
        }
    })
// ============================= add rent item code end ========================================

// ============================= delete rent item code end ===============================================
    router.get("/delete/rent/item/:id", (req, res) => {
        Rent.remove({_id: req.params.id}).then(del => {
            if(del) {
                return res.json({ message: "sucessfully deleted" })
            }
        }).catch(err => { console.log('err', err); })
    })
// ============================= delete rent item code end ===============================================

module.exports = router;