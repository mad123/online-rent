const express = require("express");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const {jwt_secret} = require("../config/kyes");

const router = express.Router();
const User = mongoose.model("User");

// ============================= register user code start ===================================
    router.post("/register/:id?", (req, res) => {
        var { name, email, password, mobile, address } = req.body;

        if(!name || !email || !password || !mobile || !address) {
            return  res.status(422).json({ error: "all fields are required" })
        } else {
            if(req.params.id) {
                User.updateOne(
                    { _id: req.params.id }, 
                    { $set: { name, email, mobile, address } } 
                ).then(up => {
                    return res.json({ success: true, message: "sucessfully Update" })
                })
            } else {
                User.findOne({email: email}).then((result) => {
                    if(result) {
                        return res.status(422).json({ error: "User Already Exits" })
                    } else {
                        bcrypt.hash(password, 12).then(password => {
                            const user = new User({ name, email, password, mobile, address });
                
                            user.save().then(user => {
                                return res.json({ success: true, message: "sucessfully register" })
                            }).catch(err => {console.log('save err', err) })
                        })
                    }
                }).catch(err => { console.log('find err', err); })
            }
        }
    })
// ============================= register user code end ==============================

// ============================= login user code start ==============================
    router.post("/login", (req, res) => {
        var { email, password } = req.body;

        if(!email || !password) {
            res.status(422).json({error: "all fields are required"})
        } else {
            User.findOne({email}).then((result) => {
                if(!result) {
                    return res.status(422).json({ error: "Invalid credential" })
                } else {
                    bcrypt.compare(password, result.password).then(doMatch => {
                        if(doMatch) {
                            const token = jwt.sign({_id:result._id}, jwt_secret);
                            const {_id, name, email } = result;
                            return res.json({ message: "Login sucessfully", token, user: {_id, name, email } })
                        } else {
                            return res.status(422).json({ error: "Invalid credential" })
                        }
                    }).catch(err => { console.log('find err', err); })
                }
            }).catch(err => { console.log('find err', err); })
        }
    })
// ============================= login user code end ===============================================

// ============================= delete user code end ===============================================
    router.get("/delete/:id", (req, res) => {
        User.remove({_id: req.params.id}).then(del => {
            if(del) {
                return res.json({ message: "User sucessfully deleted" })
            }
        }).catch(err => { console.log('err', err); })
    })
// ============================= delete user code end ===============================================

module.exports = router;