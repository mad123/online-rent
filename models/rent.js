const mongoose = require("mongoose");

const rentSchema = mongoose.Schema({
    userId: { type: String, require: true },
    itemId: { type: String, require: true },
    quantity: {type: String, require: true },
    start_date: {type: String, require: true },
    end_date: {type: String, require: true },
    rentDate: {type: String, require: true }
});

mongoose.model("Rent", rentSchema);