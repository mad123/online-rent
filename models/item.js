const mongoose = require("mongoose");

const itemSchema = mongoose.Schema({
    userId: { type: String, require: true },
    name: { type: String, require: true },
    rent_price: { type: String, require: true },
    manufacture_date: { type: String, require: true },
    actual_price: { type: String, require: true },
    created_date: { type: String, require: true },
});

mongoose.model("Item", itemSchema);