const express = require("express");
const bodyParser = require('body-parser');
const app = express();

require("./config/db");
require("./models/user")
require("./models/item")
require("./models/rent")
const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api', require("./routes/auth"));
app.use('/api', require("./routes/item"));
app.use('/api', require("./routes/rent"));

app.listen(port, () => {
    console.log("server running on port", port);
})