const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Item = mongoose.model("Item");

// ====================== get item by id or get all items start =========================
    router.get("/items/:id?", (req, res) => {
        var findData = req.params.id == undefined ? Item.find() : Item.find({_id: req.params.id});

        findData.then(items => {
            return res.json({ items })
        }).catch(err => {console.log('err', err) })
    });
// ====================== get item by id or get all items end =============================

// =========================== add item code start ========================================
    router.post("/add_item/:id?", (req, res) => {
        var { userId, name, rent_price, manufacture_date, actual_price } = req.body;

        if(!name || !rent_price || !manufacture_date || !actual_price) {
            return res.status(422).json({error: "all fields are required"});
        } else {
            if(req.params.id) {
                Item.updateOne(
                    { _id: req.params.id }, 
                    { $set: { name, rent_price, manufacture_date, actual_price } } 
                ).then(up => {
                    return res.json({ success: true, message: "Item sucessfully update" })
                })
            } else {
                const item = new Item({ userId, name, rent_price, manufacture_date, actual_price });
                item.save().then(item => {
                    return res.json({ success: true, message: "Item sucessfully added", item: item })
                }).catch(err => {console.log('save err', err) })
            }
        }
    })
// ============================= add item code end ========================================

// ============================= delete item code end ===============================================
    router.get("/delete/:id", (req, res) => {
        Item.remove({_id: req.params.id}).then(del => {
            if(del) {
                return res.json({ message: "Item sucessfully deleted" })
            }
        }).catch(err => { console.log('err', err); })
    })
// ============================= delete item code end ===============================================

module.exports = router;